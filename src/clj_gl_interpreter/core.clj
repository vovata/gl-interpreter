(ns clj-gl-interpreter.core
  (:require [taoensso.timbre :as timbre]
            [clojure.string :as s]
            [clojure.spec.alpha :as spec])
  (:gen-class))

(spec/def ::dim-number (spec/int-in 1 250))
(spec/def ::I-args (spec/coll-of ::dim-number :count 2))
(spec/def ::color-char char?)
(spec/def ::L-args (spec/cat :x ::dim-number :y ::dim-number :c ::color-char))
(spec/def ::V-args (spec/cat :x ::dim-number :y1 ::dim-number :y2 ::dim-number :c ::color-char))
(spec/def ::H-args (spec/cat :x ::dim-number :y1 ::dim-number :y2 ::dim-number :c ::color-char))
(spec/def ::F-args (spec/cat :x ::dim-number :y ::dim-number :c ::color-char))

(defn- state-with-trace [state note]
  (println note)
  state)

(defn- ->args [command-ch command-args]
  (case command-ch
    "I" (map read-string command-args)
    ("L" "V" "H" "F") (->> command-args
                           butlast
                           (map read-string)
                           (#(let [color-arg (take-last 1 command-args)]
                               (when (= 1 (count color-arg))
                                 (concat % (first color-arg))))))
    nil))

(defn- calc-checks [state x y color]
  (let [{:keys [img img-x img-y]} state]
    (filter (fn [[nx ny]]
              (and (> nx 0)
                   (> ny 0)
                   (<= nx img-x)
                   (<= ny img-y)
                   (= color (get-in img [(dec ny) (dec nx)]))))
            [[(dec x) y] [x (dec y)] [(inc x) y] [x (inc y)]])))

(defn process-line [state line]
  (let [line-args    (s/split line #" ")
        command-ch   (first line-args)
        command-args (->args command-ch (rest line-args))]
    (timbre/debug "command-ch:" command-ch ", command-args:" (pr-str command-args))
    (case command-ch
      "X" (if (empty? command-args)
            (assoc state :run false)
            (state-with-trace state "Invalid command arguments."))
      "I" (if (spec/valid? ::I-args command-args)
            (let [[m n] command-args]
              (assoc state :img (into [] (repeat n (into [] (repeat m \O))))
                           :img-x m
                           :img-y n))
            (state-with-trace state "Invalid command arguments."))
      "L" (if (spec/valid? ::L-args command-args)
            (let [[x y c] command-args]
              (if (or (> x (:img-x state))
                      (> y (:img-y state)))
                (state-with-trace state "Invalid input values.")
                (assoc-in state [:img (dec y) (dec x)] c)))
            (state-with-trace state "Invalid command arguments."))
      "V" (if (spec/valid? ::V-args command-args)
            (let [[x y1 y2 c] command-args]
              (if (or (> x (:img-x state))
                      (> y1 (:img-y state))
                      (> y2 (:img-y state))
                      (> y1 y2))
                (state-with-trace state "Invalid input values.")
                (reduce #(assoc-in %1 [:img (dec %2) (dec x)] c) state (range y1 (inc y2)))))
            (state-with-trace state "Invalid command arguments."))
      "H" (if (spec/valid? ::H-args command-args)
            (let [[x1 x2 y c] command-args]
              (if (or (> x1 (:img-x state))
                      (> x2 (:img-x state))
                      (> y (:img-y state))
                      (> x1 x2))
                (state-with-trace state "Invalid input values.")
                (reduce #(assoc-in %1 [:img (dec y) (dec %2)] c) state (range x1 (inc x2)))))
            (state-with-trace state "Invalid command arguments."))
      "F" (if (spec/valid? ::F-args command-args)
            (let [[x y c] command-args]
              (if (or (> x (:img-x state))
                      (> y (:img-y state)))
                (state-with-trace state "Invalid input values.")
                (let [color (get-in state [:img (dec y) (dec x)])]
                  (loop [new-state      state
                         pending-checks [[x y]]]
                    (if (empty? pending-checks)
                      new-state
                      (let [[fx fy] (first pending-checks)
                            rest-pending-checks (rest pending-checks)
                            new-checks          (calc-checks new-state fx fy color)]
                        (recur (assoc-in new-state [:img (dec fy) (dec fx)] c) (into rest-pending-checks new-checks))))))))
            (state-with-trace state "Invalid command arguments."))
      "S" (if (empty? command-args)
            (if-let [img (:img state)]
              (state-with-trace state (s/join "\n" (map #(s/join " " %) img)))
              (state-with-trace state "Empty image."))
            (state-with-trace state "Invalid command arguments."))
      (state-with-trace state "Invalid command."))))

(defn- main-loop
  "Main command line loop"
  [state]
  (loop [run-state state]
    (when (:run run-state)
      (print ">")
      (flush)
      (let [line (read-line)]
        (recur (process-line run-state line))))))

(defn -main
  [& args]
  (main-loop {:run true}))


