(defproject clj-gl-interpreter "0.1.0-SNAPSHOT"
  :description "Just a simple line interpreter"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.taoensso/timbre "4.10.0"]]
  :main ^:skip-aot clj-gl-interpreter.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
