(ns clj-gl-interpreter.core-test
  (:require [clojure.test :refer :all]
            [clj-gl-interpreter.core :refer :all]))

(deftest test-operations
  (with-local-vars [state {}]
    (testing "Creation"
      (var-set state (process-line @state "I 5 6"))
      (is (= [[\O \O \O \O \O]
              [\O \O \O \O \O]
              [\O \O \O \O \O]
              [\O \O \O \O \O]
              [\O \O \O \O \O]
              [\O \O \O \O \O]] (:img @state)))
      (is (= 5 (:img-x @state)))
      (is (= 6 (:img-y @state))))
    (testing "Point set"
      (var-set state (process-line @state "L 2 3 A"))
      (is (= [[\O \O \O \O \O]
              [\O \O \O \O \O]
              [\O \A \O \O \O]
              [\O \O \O \O \O]
              [\O \O \O \O \O]
              [\O \O \O \O \O]] (:img @state))))
    (testing "Fill"
      (var-set state (process-line @state "F 3 3 J"))
      (is (= [[\J \J \J \J \J]
              [\J \J \J \J \J]
              [\J \A \J \J \J]
              [\J \J \J \J \J]
              [\J \J \J \J \J]
              [\J \J \J \J \J]] (:img @state))))
    (testing "Vertical line"
      (var-set state (process-line @state "V 2 3 4 W"))
      (is (= [[\J \J \J \J \J]
              [\J \J \J \J \J]
              [\J \W \J \J \J]
              [\J \W \J \J \J]
              [\J \J \J \J \J]
              [\J \J \J \J \J]] (:img @state))))
    (testing "Horizontal line"
      (var-set state (process-line @state "H 3 4 2 Z"))
      (is (= [[\J \J \J \J \J]
              [\J \J \Z \Z \J]
              [\J \W \J \J \J]
              [\J \W \J \J \J]
              [\J \J \J \J \J]
              [\J \J \J \J \J]] (:img @state))))
    (testing "Re-creation"
      (var-set state (process-line @state "I 6 5"))
      (is (= [[\O \O \O \O \O \O]
              [\O \O \O \O \O \O]
              [\O \O \O \O \O \O]
              [\O \O \O \O \O \O]
              [\O \O \O \O \O \O]] (:img @state)))
      (is (= 6 (:img-x @state)))
      (is (= 5 (:img-y @state))))
    (testing "Huge re-creation and fill"
      (var-set state (process-line @state "I 200 200"))
      (var-set state (process-line @state "F 2 3 C"))
      (is (= (into [] (repeat 200 (into [] (repeat 200 \C)))) (:img @state)))
      (is (= 200 (:img-x @state)))
      (is (= 200 (:img-y @state))))))
